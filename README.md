# 小安科技 iOS蓝牙SDK 集成文档

## 概述及资源

蓝牙工具iOS SDK提供给集成iOS原生客户端开发的开发者使用, SDK不依赖任何第三方库。

### 环境需求

| 条目 | 资源 |
| --- | --- |
| 开发目标 | 兼容iOS8+  |
| 开发环境 | Xcode Version 11.1 (10B61) |
| 系统依赖 | CoreBluetooth.framework |
| SDK三方依赖 | 无 |


### 1.0.6 修改项

#### Features

可设置以下接口语音位和音量大小 , 原来的接口可以正常使用

```
- (void)setAcc:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;
- (void)setBackWheel:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)config;
- (void)setSaddle:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;
- (void)findCar:(blockBLEResult)callback volume:(NSInteger)volume;
- (void)stalled:(blockBLEResult)callback voiceCfg:(XCVoiceConfig *)voiceConfig;
- (void)playSound:(NSInteger)index volume:(NSInteger)volume callback:(blockBLEResult)callback;
```
#### Bugfix

修复iOS 13 系统无法成功连接的问题

### 1.0.4 修改项
支持 **setAPN**
### 1.0.3 修改项

* Configuration 去掉了Default方法。 改为init时默认进行操作
* 蓝牙回复操作中CodeString 字段名变为 Status
* 新增 ConnectToIMEI 的方法，加入新的参数以控制发现设备后是否继续扫描设备。

### 接口

-------
* **初始化**

```
/**
 单例模式

 @return 返回一个XCBLEMain的实例对象
 */
+ (instancetype)shared;
```

-------
* **配置基础信息**

```
/**
 设置蓝牙配置

 @param configuration 配置项
 */
-(void)configurtion:(XCBLEConfiguration *)configuration;
```
* **示例**

```

- (void)viewDidLoad {
    [super viewDidLoad];
    XCBLEConfiguration *configuration = [[XCBLEConfiguration alloc] init];
    [[XCBLEMain shared] configurtion:configuration];
}
```

-------
* **设置蓝牙状态监听**

```

/**
 设置设备状态监听

 @param stateCallback 设备状态callback
 */
-(void)setBlockOnCentralManagerDidUpdateState:(XCBLECentralManagerDidUpdateStateBlock)stateCallback;

```
* **示例**

```
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __weak typeof(self)weakSelf = self;

    [[XCBLEMain shared] setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        switch (central.state) {
            case CBManagerStateUnknown:
                [weakSelf.connectButton setTitle:@"蓝牙状态未知" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateResetting:
                [weakSelf.connectButton setTitle:@"蓝牙重启中" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStatePoweredOff:
                [weakSelf.connectButton setTitle:@"蓝牙没开,请开蓝牙" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateUnsupported:
                [weakSelf.connectButton setTitle:@"不支持蓝牙" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateUnauthorized:
                [weakSelf.connectButton setTitle:@"未授权蓝牙" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStatePoweredOn:
                [weakSelf.connectButton setTitle:@"连接设备" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = true;
                break;
        }
    }];
    
}

```


-------

* **设置断连监听**

```
/**
 设置以监听断连

 @param callback 返回结果
 */
-(void)setBlockDisconnect:(XCBLEDisconnectBlock)callback;
```

* **示例**

```
- (void)viewDidLoad {
    [super viewDidLoad];
    [[XCBLEMain shared] setBlockDisconnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [weakSelf.connectButton setTitle:@"断开连接,点击重连" forState:UIControlStateNormal];
    }];
}
```


### 设备操作

* **连接设备**

```
/**
  连接到指定IMEI设备

 @param imei 指定的设备号 已经包含了发现服务
 @param fail 连接失败 callback
 @param deviceReadyCallback 连接成功可操作 并且主动停止扫描
 */
-(void)connectToIMEI:(NSString *)imei deviceReadyCallback:(blockBLEResult)deviceReadyCallback fail:(XCBLEFailToConnectBlock)fail;

/**
 连接到指定IMEI设备
 
 @param imei 指定的设备号 已经包含了发现服务
 @param cscan Continue scann 连接后停止扫描 默认false 停止  true 不停止
 @param fail 连接失败 callback
 @param deviceReadyCallback 连接成功可操作
 */
-(void)connectToIMEI:(NSString *)imei cscan:(BOOL)cscan deviceReadyCallback:(blockBLEResult)deviceReadyCallback fail:(XCBLEFailToConnectBlock)fail;
```

-------

* **控制电门**

```
/**
 BLE控制电门

 @param on 0:熄火 1:点火
 */
- (void)setAcc:(BOOL)on callback:(blockBLEResult)callback;


/**
 BLE控制车辆启动和设防

 @param on 0:车辆启动 1:车辆设防
 */
- (void)setDefend:(BOOL)on callback:(blockBLEResult)callback;

/**
 BLE控制车辆后轮锁

 @param on 0:后轮打开 1:后轮上锁
 */
- (void)setBackWheel:(BOOL)on callback:(blockBLEResult)callback ;

/**
 BLE控制车辆电池仓锁

 @param on 0:电池仓打开 1:电池仓上锁
 */
- (void)setSaddle:(BOOL)on callback:(blockBLEResult)callback ;

/**
 播放寻车声
 */
- (void)findCar:(blockBLEResult)callback;


/**
 获取车辆状态
 */
- (void)getStatus:(blockBLEResult)callback;


/**
 重启
 */
- (void)restart:(blockBLEResult)callback;


/**
 关机
 */
- (void)shutdown:(blockBLEResult)callback;


/**
 推车  = 撤防+熄火

 @param callback 回调
 */
- (void)stalled:(blockBLEResult)callback;


/**
 播放语音
 @param index 语音对应的Index
 */
- (void)playSound:(NSInteger)index callback:(blockBLEResult)callback;

/**
 BLE配置APN名称

 @param 'APN,name,passwd' 其中name 和 password 可以为空 ""
 @param callback 结果回调
 */
- (void)setAPN:(NSString *)apnName userName:(NSString *)userName password:(NSString *)password callback:(blockBLEResult)callback;

```


-------

### 蓝牙操作方法

```
/**
 获取蓝牙适配器状态 所有非开状态返回false 需要先setBlockOnCentralManagerDidUpdateState

 @return 蓝牙适配器状态
 */
- (BOOL)isBluetoothAdapterOn;




/**
 设置连接设备的RSSI监听 该项需要在 Configuration 中配置rssiInterval 默认为0 关闭

 @param callback 从callback中返回
 */
-(void)setBlockReadRSSI:(XCBLEDidReadRSSIBlock)callback;


/**
 设置发现设备的监听
 
 @param block 在block中执行对应操作
 */
- (void)startDiscoverToPeripherals:(XCBLEDiscoverPeripheralsBlock)block;


//断开所有已连接的设备
- (void)cancelAllPeripheralsConnection;


/**
 停止扫描
 */
- (void)stopScan;

```


* **blockBLEResult回复格式示例**


```
//只有getStatus 接口会回复result,其他操作只会回复code 和 status
{
    code: 0,
    status: "成功",
    result: {
        "acc": 0,
        "defend": 1,
        "gsm": 25,
        "mode": 0,
        "seatLock": 1,
        "voltageMv": 47174,
        "wheelLock": 1
    }
}
```



### 相关资源

**XCBLEConfiguration 配置**

* shortServiceUUIDs     设置扫描的参数
* server_tx_uuid        提供读取特征码的参数
* server_rx_uuid        提供写入服务的参数
* service_uuid          提供的service_uuid
* token                 提供的token
* rssiInterval          连接上后读取RSSI值的时间间隔


**ErrorCode**

```

typedef enum : NSUInteger {
    BleErrorCode_success = 0,               //成功
    BleErrorCode_error_token = 1,           //token 错误
    BleErrorCode_error_content = 2,         //请求内容错误
    BleErrorCode_error_cmd = 3,             //命令字错误
    BleErrorCode_error_operation = 4,       //操作失败
    BleErrorCode_error_unsupport = 5,       //不支持的操作
    BleErrorCode_error_moving = 6,          //车辆行驶中
    BleErrorCode_error_timeout = 7,         //响应超时
    BleErrorCode_error_checkRSP = 8,        //回复了错误的内容
    BleErrorCode_error_unknow = 9,          //未知错误
} BleErrorCode;[]()


```


## FAQ

1. 连接不上蓝牙
    * 请确认设备在开启状态，且正在广播，可以用nRF或者LightBlue等蓝牙工具测试。
    * 请确认连接的是设备IMEI号，以868开头的15位设备号码。
    * 由于**connectToIMEI** 方法没有做时间限制，故如果设备不在附近的且在前台的情况下会一直进行蓝牙扫描直到扫描到为止。如有需要可在上层进行时间控制。
2. **startDiscoverToPeripherals**方法
    * startDiscoverToPeripherals会不停扫描周围所有的设备，扫描选项由**XCBLEConfiguration**中**shortServiceUUIDs**控制。不设置会默认扫描所有的小安设备
    * 在 **startDiscoverToPeripherals** 中寻找到需要的设备后 需要手动进行**StopScan**以停止扫描。以免多次扫到重复调用的问题。
    * iOS下扫到的数据携带在设备的广播数据中,需要自己进行校验。携带的设备号为12位。例如IMEI为**868183030470848** 广播中数据则为**183030470848**。

3. **isBluetoothAdapterOn** 方法
    * 由于iOS底层机制 直接调用isBluetoothAdapterOn是没有任何作用的。需要监听蓝牙适配器状态必须先设置**setBlockOnCentralManagerDidUpdateState** 。该监听会默认调用一次，以便进行UI渲染。此后可以开始使用isBluetoothAdapterOn方法。