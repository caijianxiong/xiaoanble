# Xiaoan Technology iOS Bluetooth SDK Integration Document

## Overview and resources

The Bluetooth tool iOS SDK is provided to developers who integrate iOS native client development. The SDK does not rely on any third-party libraries.

### Environmental requirements

| Entry | Resources |
| --- | --- |
| Development goals | Compatible with iOS8+ |
| Development Environment | Xcode Version 11.1 (10B61) |
| System dependencies | CoreBluetooth.framework |
| SDK three-party dependency | None |


### 1.0.6 Modification

#### Features

You can set the voice bit and volume of the following interfaces, and the original interface can be used normally

```
-(void)setAcc:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;
-(void)setBackWheel:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)config;
-(void)setSaddle:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;
-(void)findCar:(blockBLEResult)callback volume:(NSInteger)volume;
-(void)stalled:(blockBLEResult)callback voiceCfg:(XCVoiceConfig *)voiceConfig;
-(void)playSound:(NSInteger)index volume:(NSInteger)volume callback:(blockBLEResult)callback;
```
#### Bugfix

Fix the problem that the iOS 13 system cannot connect successfully

### 1.0.4 Modified items
Support **setAPN**
### 1.0.3 Modification

* Configuration removes the Default method. Default operation when changing to init
* The CodeString field name becomes Status in the Bluetooth reply operation
* Added the ConnectToIMEI method, adding new parameters to control whether to continue scanning the device after the device is discovered.

### Interface

-------
* **Initialization**

```
/**
 Singleton mode

 @return returns an instance of XCBLEMain
 */
+ (instancetype)shared;
```

-------
* **Configuration basic information**

```
/**
 Set up Bluetooth configuration

 @param configuration configuration item
 */
-(void)configurtion:(XCBLEConfiguration *)configuration;
```
* **Example**

```

-(void)viewDidLoad {
    [super viewDidLoad];
    XCBLEConfiguration *configuration = [[XCBLEConfiguration alloc] init];
    [[XCBLEMain shared] configurtion:configuration];
}
```

-------
* **Set up Bluetooth status monitoring**

```

/**
 Set up device status monitoring

 @param stateCallback device state callback
 */
-(void)setBlockOnCentralManagerDidUpdateState:(XCBLECentralManagerDidUpdateStateBlock)stateCallback;

```
* **Example**

```
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __weak typeof(self)weakSelf = self;

    [[XCBLEMain shared] setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        switch (central.state) {
            case CBManagerStateUnknown:
                [weakSelf.connectButton setTitle:@"Bluetooth state unknown" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateResetting:
                [weakSelf.connectButton setTitle:@"Bluetooth restarting" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStatePoweredOff:
                [weakSelf.connectButton setTitle:@"Bluetooth is not turned on, please turn on Bluetooth" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateUnsupported:
                [weakSelf.connectButton setTitle:@"Bluetooth is not supported" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateUnauthorized:
                [weakSelf.connectButton setTitle:@"Unauthorized Bluetooth" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStatePoweredOn:
                [weakSelf.connectButton setTitle:@"Connect Device" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = true;
                break;
        }
    }];
    
}

```


-------

* **Set up disconnection monitoring**

```
/**
 Set to listen for disconnection

 @param callback returns the result
 */
-(void)setBlockDisconnect:(XCBLEDisconnectBlock)callback;
```

* **Example**

```
-(void)viewDidLoad {
    [super viewDidLoad];
    [[XCBLEMain shared] setBlockDisconnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [weakSelf.connectButton setTitle:@"Disconnect, click to reconnect" forState:UIControlStateNormal];
    }];
}
```


### Equipment Operation

* **Connect Device**

```
/**
  Connect to the specified IMEI device

 The device number specified by @param imei already includes the discovery service
 @param fail connection failed callback
 @param deviceReadyCallback is successfully connected and operable, and actively stops scanning
 */
-(void)connectToIMEI:(NSString *)imei deviceReadyCallback:(blockBLEResult)deviceReadyCallback fail:(XCBLEFailToConnectBlock)fail;

/**
 Connect to the specified IMEI device
 
 The device number specified by @param imei already includes the discovery service
 @param cscan Continue scann Stop scanning after connecting. Default false. Stop true. Don’t stop.
 @param fail connection failed callback
 @param deviceReadyCallback is successfully connected and operable
 */
-(void)connectToIMEI:(NSString *)imei cscan:(BOOL)cscan deviceReadyCallback:(blockBLEResult)deviceReadyCallback fail:(XCBLEFailToConnectBlock)fail;
```

-------

* **Control switch**

```
/**
 BLE control switch

 @param on 0: Turn off the flame 1: Ignite
 */
-(void)setAcc:(BOOL)on callback:(blockBLEResult)callback;


/**
 BLE control vehicle start and fortification

 @param on 0: vehicle start 1: vehicle fortification
 */
-(void)setDefend:(BOOL)on callback:(blockBLEResult)callback;

/**
 BLE control vehicle rear wheel lock

 @param on 0: the rear wheel is open 1: the rear wheel is locked
 */
-(void)setBackWheel:(BOOL)on callback:(blockBLEResult)callback;

/**
 BLE control vehicle battery compartment lock

 @param on 0: The battery compartment is open 1: The battery compartment is locked
 */
-(void)setSaddle:(BOOL)on callback:(blockBLEResult)callback;

/**
 Play car search sound
 */
-(void)findCar:(blockBLEResult)callback;


/**
 Get vehicle status
 */
-(void)getStatus:(blockBLEResult)callback;


/**
 Reboot
 */
-(void)restart:(blockBLEResult)callback;


/**
 Shutdown
 */
-(void)shutdown:(blockBLEResult)callback;


/**
 Cart = disarm + flameout

 @param callback callback
 */
-(void)stalled:(blockBLEResult)callback;


/**
 Play voice
 @param index Index corresponding to the voice
 */
-(void)playSound:(NSInteger)index callback:(blockBLEResult)callback;

/**
 BLE configuration APN name

 @param'APN,name,passwd' where name and password can be empty ""
 @param callback result callback
 */
-(void)setAPN:(NSString *)apnName userName:(NSString *)userName password:(NSString *)password callback:(blockBLEResult)callback;

```


-------

### Bluetooth operation method

```
/**
 Get the status of the Bluetooth adapter. All non-open states return false, you need to setBlockOnCentralManagerDidUpdateState first

 @return Bluetooth adapter status
 */
-(BOOL)isBluetoothAdapterOn;




/**
 Set the RSSI monitoring of the connected device. This item needs to be configured in Configuration. The default is 0. Off

 @param callback returns from callback
 */
-(void)setBlockReadRSSI:(XCBLEDidReadRSSIBlock)callback;


/**
 Set up listening for discovered devices
 
 @param block performs corresponding operations in the block
 */
-(void)startDiscoverToPeripherals:(XCBLEDiscoverPeripheralsBlock)block;


//Disconnect all connected devices
-(void)cancelAllPeripheralsConnection;


/**
 Stop scanning
 */
-(void)stopScan;

```


* **BlockBLEResult reply format example**


```
//Only getStatus interface will reply result, other operations will only reply code and status
{
    code: 0,
    status: "Success",
    result: {
        "acc": 0,
        "defend": 1,
        "gsm": 25,
        "mode": 0,
        "seatLock": 1,
        "voltageMv": 47174,
        "wheelLock": 1
    }
}
```



### related resources

**XCBLEConfiguration Configuration**

* shortServiceUUIDs set scan parameters
* server_tx_uuid provides parameters for reading feature codes
* server_rx_uuid provides parameters for writing services
* service_uuid provided by service_uuid
* token provided by token
* rssiInterval The time interval to read the RSSI value after connecting


**ErrorCode**

```

typedef enum: NSUInteger {
    BleErrorCode_success = 0, //success
    BleErrorCode_error_token = 1, //token error
    BleErrorCode_error_content = 2, //Request content error
    BleErrorCode_error_cmd = 3, //Command word error
    BleErrorCode_error_operation = 4, //The operation failed
    BleErrorCode_error_unsupport = 5, //Unsupported operation
    BleErrorCode_error_moving = 6, //The vehicle is moving
    BleErrorCode_error_timeout = 7, //Response timeout
    BleErrorCode_error_checkRSP = 8, //Replied with the wrong content
    BleErrorCode_error_unknow = 9, //Unknown error
} BleErrorCode;[]()


```


## FAQ

1. Cannot connect to Bluetooth
    * Please make sure the device is turned on and broadcasting, you can use nRF or LightBlue and other Bluetooth tools to test.
    * Please make sure the IMEI number of the device connected is the 15-digit device number starting with 868.
    * Since the **connectToIMEI** method does not have a time limit, if the device is not nearby and in the foreground, it will continue to scan for Bluetooth until it is scanned. Time control can be performed on the upper level if necessary.
2. **startDiscoverToPeripherals** method
    * startDiscoverToPeripherals will continuously scan all surrounding devices, and the scanning options are controlled by **shortServiceUUIDs** in **XCBLEConfiguration**. If not set, all Xiaoan devices will be scanned by default
    * After finding the required device in **startDiscoverToPeripherals**, you need to manually perform **StopScan** to stop scanning. In order to avoid the problem of repeated calls multiple times.
    * The data scanned under iOS is carried in the broadcast data of the device and needs to be verified by yourself. The device number carried is 12 digits. For example, the IMEI is **868183030470848** and the broadcast data is **183030470848**.

3. **isBluetoothAdapterOn** method
    * Due to the underlying mechanism of iOS, calling isBluetoothAdapterOn directly has no effect. To monitor the status of the Bluetooth adapter, you must first set **setBlockOnCentralManagerDidUpdateState**. The listener will be called once by default for UI rendering. After that, you can start using the isBluetoothAdapterOn method.