//
//  XCBLEDefine.h
//  BLETest
//
//  Created by 蔡剑雄 on 30/08/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//


/*
 @brief 预定义一些库的行为和配置
 */

#ifndef XCBLEDefine_h
#define XCBLEDefine_h


//蓝牙是否打印日志 默认开启   1打印 0不打印
#define XCBLE_IS_SHOW_LOG 1

#define XCBLE_DETAULT_CHANNEL @"xcBLEDefault"

//CBcentralManager等待设备打开次数
# define XCBLE_CENTRAL_MANAGER_INIT_WAIT_TIMES 5

//CBcentralManager等待设备打开间隔时间
# define XCBLE_CENTRAL_MANAGER_INIT_WAIT_SECOND 2.0

//系统通知

//centralManager status did change notification
#define XCBLENotificationAtCentralManagerDidUpdateState @"XCNotificationAtCentralManagerDidUpdateState"
//did dicover peripheral notification
#define XCBLENotificationAtDidDiscoverPeripheral        @"XCBLENotificationAtDidDiscoverPeripheral"
//did connection peripheral notification
#define XCBLENotificationAtDidConnectPeripheral @"XCBLENotificationAtDidConnectPeripheral"
//did filed connect peripheral notification
#define XCBLENotificationAtDidFailToConnectPeripheral @"XCBLENotificationAtDidFailToConnectPeripheral"
//did disconnect peripheral notification
#define XCBLENotificationAtDidDisconnectPeripheral @"XCBLENotificationAtDidDisconnectPeripheral"
//did discover service notification
#define XCBLENotificationAtDidDiscoverServices @"XCBLENotificationAtDidDiscoverServices"
//did discover characteristics notification
#define XCBLENotificationAtDidDiscoverCharacteristicsForService @"XCBLENotificationAtDidDiscoverCharacteristicsForService"
//did read or notify characteristic when received value  notification
#define XCBLENotificationAtDidUpdateValueForCharacteristic @"XCBLENotificationAtDidUpdateValueForCharacteristic"
//did write characteristic and response value notification
#define XCBLENotificationAtDidWriteValueForCharacteristic @"XCBLENotificationAtDidWriteValueForCharacteristic"
//did change characteristis notify status notification
#define XCBLENotificationAtDidUpdateNotificationStateForCharacteristic @"XCBLENotificationAtDidUpdateNotificationStateForCharacteristic"
//did read rssi and receiced value notification
#define XCBLENotificationAtDidReadRSSI @"XCBLENotificationAtDidReadRSSI"

//蓝牙扩展通知
// did centralManager enable notification
#define XCBLENotificationAtCentralManagerEnable @"XCBLENotificationAtCentralManagerEnable"


# pragma mark - XCBLE 定义的方法

#define XCBLELog(fmt, ...) if(XCBLE_IS_SHOW_LOG){ NSLog(fmt,##__VA_ARGS__); }



typedef enum : NSUInteger {
    BleErrorCode_success = 0,               //成功
    BleErrorCode_error_token = 1,           //token 错误
    BleErrorCode_error_content = 2,         //请求内容错误
    BleErrorCode_error_cmd = 3,             //命令字错误
    BleErrorCode_error_operation = 4,       //操作失败
    BleErrorCode_error_unsupport = 5,       //不支持的操作
    BleErrorCode_error_moving = 6,          //车辆行驶中
    BleErrorCode_error_timeout = 7,         //响应超时
    BleErrorCode_error_checkRSP = 8,        //回复了错误的内容
    BleErrorCode_error_unknow = 9,          //未知错误
} BleErrorCode;



#endif /* XCBLEDefine_h */
