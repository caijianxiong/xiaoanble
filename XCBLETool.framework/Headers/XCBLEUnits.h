//
//  XCBELTool.h
//  BLETest
//
//  Created by mac on 2018/11/8.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XCBLEUnits : NSObject


+(instancetype) shareInstance ;


/**
 对RSSI进行过滤

 @param RSSI 原有的RSSI
 @return 过滤后的RSSI将会以较为稳定的值呈现
 */
- (NSNumber *)filterRSSI:(NSNumber *)RSSI;




/**
 根据设备号对RSSI进行过滤

 @param RSSI 原有的RSSI
 @param deviceId 对应设备ID
 @return 过滤后的RSSI
 */
- (NSNumber *)filterRSSI:(NSNumber *)RSSI deviceId:(NSString *)deviceId;


- (void) removeAllDeviceRSSI;

- (void) removeRSSIForDeivceId:(NSString *)deviceId;


@end

NS_ASSUME_NONNULL_END
