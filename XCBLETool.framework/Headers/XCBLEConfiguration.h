//
//  XCBLEConfiguration.h
//  BLETest
//
//  Created by 蔡剑雄 on 03/09/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//  @brief 配置参数

#import <Foundation/Foundation.h>

@interface XCBLEConfiguration:NSObject

@property (nonatomic,copy) NSArray *shortServiceUUIDs; //扫

@property (nonatomic,copy) NSString *server_tx_uuid;  //读

@property (nonatomic,copy) NSString *server_rx_uuid;  //写

@property (nonatomic,copy) NSString *service_uuid;

@property (nonatomic,copy) NSArray *token;

@property (nonatomic, assign) NSInteger intToken;

@property (nonatomic,assign) NSTimeInterval rssiInterval;  //读取连接设备的RSSI值的时间间隔 默认为0

@end



@interface XCBLEConfigurationBuilder : NSObject

@property (nonatomic,strong) XCBLEConfiguration *configuration;

@property (nonatomic,strong) XCBLEConfigurationBuilder *(^buildConfigruation)();
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setShortServiceUUIDs)(NSArray<NSString *> *shortServiceUUIDs);
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setServer_rx_uuid)(NSString *server_rx_uuid);
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setServer_tx_uuid)(NSString *server_tx_uuid);
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setService_uuid)(NSString *service_uuid);
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setToken)(NSArray<NSString *> *token);
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setRssiInterval)(NSTimeInterval rssiInterval);
@property (nonatomic,strong) XCBLEConfigurationBuilder *(^setIntToken)(NSInteger setIntToken);

@end
