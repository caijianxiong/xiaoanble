//
//  XCBLEResponesModel.h
//  BLETest
//
//  Created by 蔡剑雄 on 06/09/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XCBLEDefine.h"

@interface XCBLEResponesModel : NSObject

@property (nonatomic,assign) BleErrorCode code;
@property (nonatomic,copy)   NSString *status; /*code 码文字描述*/
@property (nonatomic,strong) NSDictionary *reuslt;

@end
