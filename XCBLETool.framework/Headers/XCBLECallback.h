//
//  XCBLECallback.h
//  BLETest
//
//  Created by 蔡剑雄 on 30/08/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
@class XCBLEOptions;

//设备状态改变的block
typedef void(^XCBLECentralManagerDidUpdateStateBlock)(CBCentralManager *central);
//找到设备的block
typedef void(^XCBLEDiscoverPeripheralsBlock)(CBCentralManager *central,CBPeripheral *peripheral,NSDictionary *advertisemenData,NSNumber *RSSI);
//连接成功的block
typedef void (^XCBLEConnectedPeripheralBlock)(CBCentralManager *central,CBPeripheral *peripheral);
//连接设备失败的block
typedef void (^XCBLEFailToConnectBlock)(CBCentralManager *central,CBPeripheral *peripheral,NSError *error);
//断开设备连接的bock
typedef void (^XCBLEDisconnectBlock)(CBCentralManager *central,CBPeripheral *peripheral,NSError *error);
//找到服务的block
typedef void (^XCBLEDiscoverServicesBlock)(CBPeripheral *peripheral,NSError *error);
//找到Characteristics的block
typedef void (^XCBLEDiscoverCharacteristicsBlock)(CBPeripheral *peripheral,CBService *service,NSError *error);
//更新（获取）Characteristics的value的block
typedef void (^XCBLEReadValueForCharacteristicBlock)(CBPeripheral *peripheral,CBCharacteristic *characteristic,NSError *error);
//获取Characteristics的名称
typedef void (^XCBLEDiscoverDescriptorsForCharacteristicBlock)(CBPeripheral *peripheral,CBCharacteristic *service,NSError *error);
//获取Descriptors的值
typedef void (^XCBLEReadValueForDescriptorsBlock)(CBPeripheral *peripheral,CBDescriptor *descriptor,NSError *error);
//cancelScanBlock方法调用后的回调
typedef void (^XCBLECancelScanBlock)(CBCentralManager *centralManager);
//cancelAllPeripheralsConnection 方法调用后的回调
typedef void (^XCBLECancelAllPeripheralsConnectionBlock)(CBCentralManager *centralManager);

typedef void (^XCBLEDidWriteValueForCharacteristicBlock)(CBCharacteristic *characteristic,NSError *error);

typedef void (^XCBLEDidWriteValueForDescriptorBlock)(CBDescriptor *descriptor,NSError *error);

typedef void (^XCBLEDidUpdateNotificationStateForCharacteristicBlock)(CBCharacteristic *characteristic,NSError *error);

typedef void (^XCBLEDidReadRSSIBlock)(NSNumber *RSSI,NSError *error);

typedef void (^XCBLEDidDiscoverIncludedServicesForServiceBlock)(CBService *service,NSError *error);

typedef void (^XCBLEDidUpdateNameBlock)(CBPeripheral *peripheral);

typedef void (^XCBLEDidModifyServicesBlock)(CBPeripheral *peripheral,NSArray *invalidatedServices);



@interface XCBLECallback : NSObject

#pragma mark - callback block
//设备状态改变的委托
@property (nonatomic, copy) XCBLECentralManagerDidUpdateStateBlock blockOnCentralManagerDidUpdateState;
//发现peripherals
@property (nonatomic, copy) XCBLEDiscoverPeripheralsBlock blockOnDiscoverPeripherals;
//连接callback
@property (nonatomic, copy) XCBLEConnectedPeripheralBlock blockOnConnectedPeripheral;
//连接设备失败的block
@property (nonatomic, copy) XCBLEFailToConnectBlock blockOnFailToConnect;
//断开设备连接的bock
@property (nonatomic, copy) XCBLEDisconnectBlock blockOnDisconnect;
//发现services
@property (nonatomic, copy) XCBLEDiscoverServicesBlock blockOnDiscoverServices;
//发现Characteristics
@property (nonatomic, copy) XCBLEDiscoverCharacteristicsBlock blockOnDiscoverCharacteristics;
//发现更新Characteristics的
@property (nonatomic, copy) XCBLEReadValueForCharacteristicBlock blockOnReadValueForCharacteristic;
//获取Characteristics的名称
@property (nonatomic, copy) XCBLEDiscoverDescriptorsForCharacteristicBlock blockOnDiscoverDescriptorsForCharacteristic;
//获取Descriptors的值
@property (nonatomic,copy) XCBLEReadValueForDescriptorsBlock blockOnReadValueForDescriptors;

@property (nonatomic, copy) XCBLEDidWriteValueForCharacteristicBlock blockOnDidWriteValueForCharacteristic;

@property (nonatomic, copy) XCBLEDidWriteValueForDescriptorBlock blockOnDidWriteValueForDescriptor;

@property (nonatomic, copy) XCBLEDidUpdateNotificationStateForCharacteristicBlock blockOnDidUpdateNotificationStateForCharacteristic;

@property (nonatomic, copy) XCBLEDidReadRSSIBlock blockOnDidReadRSSI;

@property (nonatomic, copy) XCBLEDidDiscoverIncludedServicesForServiceBlock blockOnDidDiscoverIncludedServicesForService;

@property (nonatomic, copy) XCBLEDidUpdateNameBlock blockOnDidUpdateName;

@property (nonatomic, copy) XCBLEDidModifyServicesBlock blockOnDidModifyServices;


//Bluettooth stopScan方法调用后的回调
@property(nonatomic,copy) XCBLECancelScanBlock blockOnCancelScan;
//Bluettooth stopConnectAllPerihperals 方法调用后的回调
@property(nonatomic,copy) XCBLECancelAllPeripheralsConnectionBlock blockOnCancelAllPeripheralsConnection;
//Bluettooth 蓝牙使用的参数参数
@property(nonatomic,strong) XCBLEOptions *bleOptions;


#pragma mark - 过滤器Filter
//发现peripherals规则
@property (nonatomic, copy) BOOL (^filterOnDiscoverPeripherals)(NSString *peripheralName, NSDictionary *advertisementData, NSNumber *RSSI);
//连接peripherals规则
@property (nonatomic, copy) BOOL (^filterOnconnectToPeripherals)(NSString *peripheralName, NSDictionary *advertisementData, NSNumber *RSSI);



@end
