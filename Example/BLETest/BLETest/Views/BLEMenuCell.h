//
//  BLEMenuCell.h
//  BLETest
//
//  Created by jianxiongc on 2018/12/4.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BLEMenuCell : UICollectionViewCell

@property (nonatomic,strong) NSDictionary *cellModel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
