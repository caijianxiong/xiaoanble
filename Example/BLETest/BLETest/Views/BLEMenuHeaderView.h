//
//  BLEMenuHeaderView.h
//  BLETest
//
//  Created by jianxiongc on 2018/12/4.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BLEMenuHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *voltageLabel;
@property (weak, nonatomic) IBOutlet UILabel *rssiLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

NS_ASSUME_NONNULL_END
