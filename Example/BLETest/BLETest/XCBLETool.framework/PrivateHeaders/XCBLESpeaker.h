//
//  XCBLESpeaker.h
//  BLETest
//
//  Created by 蔡剑雄 on 30/08/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XCBLECallback.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface XCBLESpeaker : NSObject


- (XCBLECallback *)callback;
- (XCBLECallback *)callbackOnCurrChannel;
- (XCBLECallback *)callbackOnChnnel:(NSString *)channel;
- (XCBLECallback *)callbackOnChnnel:(NSString *)channel
                createWhenNotExist:(BOOL)createWhenNotExist;

//切换频道
- (void)switchChannel:(NSString *)channel;

//添加到notify list
- (void)addNotifyCallback:(CBCharacteristic *)c
                withBlock:(void(^)(CBPeripheral *peripheral, CBCharacteristic *characteristics, NSError *error))block;

//添加到notify list
- (void)removeNotifyCallback:(CBCharacteristic *)c;

//获取notify list
- (NSMutableDictionary *)notifyCallBackList;

//获取notityBlock
- (void(^)(CBPeripheral *peripheral, CBCharacteristic *characteristics, NSError *error))notifyCallback:(CBCharacteristic *)c;


@end
