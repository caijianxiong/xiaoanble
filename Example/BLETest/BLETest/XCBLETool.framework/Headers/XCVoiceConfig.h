//
//  XCVoiceConfig.h
//  BLETest
//
//  Created by jianxiongc on 2019/10/17.
//  Copyright © 2019 蔡剑雄. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XCVoiceConfig : NSObject

@property (nonatomic,assign, readonly) NSInteger idx;

@property (nonatomic,assign, readonly) NSInteger volume;

- (void)voiceConfig:(NSInteger)idx;

- (void)voiceConfig:(NSInteger)idx volume:(NSInteger)volume;

- (instancetype)init:(NSInteger)idx NS_DESIGNATED_INITIALIZER;

- (instancetype)init:(NSInteger)idx volume:(NSInteger)volume NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype) new;
@end

NS_ASSUME_NONNULL_END
