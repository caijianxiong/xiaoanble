//
//  XCBLETool.h
//  XCBLETool
//
//  Created by 蔡剑雄 on 06/09/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCBLEMain.h"
#import "XCBLEConfiguration.h"
#import "XCBLEResponesModel.h"
#import "XCBLECallback.h"
#import "XCBLEOptions.h"
#import "XCBLEUnits.h"
#import "XCVoiceConfig.h"

//! Project version number for XCBLETool.
FOUNDATION_EXPORT double XCBLEToolVersionNumber;

//! Project version string for XCBLETool.
FOUNDATION_EXPORT const unsigned char XCBLEToolVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XCBLETool/PublicHeader.h>


