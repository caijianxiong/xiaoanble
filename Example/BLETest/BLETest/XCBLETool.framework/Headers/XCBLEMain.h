//
//  XCBLEMain.h
//  BLETest
//
//  Created by 蔡剑雄 on 03/09/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XCBLECallback.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "XCBLEConfiguration.h"

@class XCBLEResponesModel;
@class XCVoiceConfig;

typedef void(^blockBLEResult)(XCBLEResponesModel *result);

@interface XCBLEMain : NSObject



/**
 单例模式

 @return 返回一个XCBLEMain的实例对象
 */
+ (instancetype)shared;


/**
 设置蓝牙配置

 @param configuration 配置项
 */
-(void)configurtion:(XCBLEConfiguration *)configuration;


/**
  连接到指定IMEI设备

 @param imei 指定的设备号 已经包含了发现服务
 @param fail 连接失败 callback
 @param deviceReadyCallback 连接成功可操作 并且主动停止扫描
 */
-(void)connectToIMEI:(NSString *)imei deviceReadyCallback:(blockBLEResult)deviceReadyCallback fail:(XCBLEFailToConnectBlock)fail;


/**
 连接到指定IMEI设备
 
 @param imei 指定的设备号 已经包含了发现服务
 @param cscan Continue scann 连接后停止扫描 默认false 停止  true 不停止
 @param fail 连接失败 callback
 @param deviceReadyCallback 连接成功可操作
 */
-(void)connectToIMEI:(NSString *)imei cscan:(BOOL)cscan deviceReadyCallback:(blockBLEResult)deviceReadyCallback fail:(XCBLEFailToConnectBlock)fail;


/**
 设置设备状态监听

 @param stateCallback 设备状态callback
 */
-(void)setBlockOnCentralManagerDidUpdateState:(XCBLECentralManagerDidUpdateStateBlock)stateCallback;


/**
 设置以监听断连

 @param callback 返回结果
 */
-(void)setBlockDisconnect:(XCBLEDisconnectBlock)callback;


//断开所有已连接的设备
- (void)cancelAllPeripheralsConnection;


/**
 停止扫描
 */
- (void)stopScan;



/**
 BLE控制电门

 @param on 0:熄火 1:点火
 */
- (void)setAcc:(BOOL)on callback:(blockBLEResult)callback;
- (void)setAcc:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;


/**
 BLE控制车辆启动和设防

 @param on 0:车辆启动 1:车辆设防
 @param callback 操作回调
 @param voiceConfig 语音位和音量
 @param isIgnoreFence 是否启动围栏失能
 */
- (void)setDefend:(BOOL)on callback:(blockBLEResult)callback;
- (void)setDefend:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceCfg;
- (void)setDefend:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceCfg isIgnoreFence:(BOOL)isIgnoreFence;
/**
 BLE控制车辆后轮锁

 @param on 1:后轮打开 0:后轮上锁
 */
- (void)setBackWheel:(BOOL)on callback:(blockBLEResult)callback ;
- (void)setBackWheel:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)config;


/**
 BLE控制车辆电池仓锁

 @param on 1:电池仓打开 0:电池仓上锁
 */
- (void)setSaddle:(BOOL)on callback:(blockBLEResult)callback ;
- (void)setSaddle:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;


/**
 设置存储模式
 
 @param on 1:设置 0:关闭
 */
- (void)setStoreModel:(BOOL)on callback:(blockBLEResult)callback voiceConfig:(XCVoiceConfig *)voiceConfig;


/**
 播放寻车声volume
 */
- (void)findCar:(blockBLEResult)callback;
- (void)findCar:(blockBLEResult)callback volume:(NSInteger)volume;

/**
 获取车辆状态
 */
- (void)getStatus:(blockBLEResult)callback;

/**
 获取设备GPS信息
 */
- (void)getGPSInfo:(blockBLEResult)callback;

/**
 获取设备详细信息
 */
- (void)getDetailInfo:(blockBLEResult)callback;
- (void)getDetailInfoV2:(blockBLEResult)callback;


/**
 重启
 */
- (void)restart:(blockBLEResult)callback;


/**
 关机
 */
- (void)shutdown:(blockBLEResult)callback;


/**
 推车  = 撤防+熄火

 @param callback 回调
 */
- (void)stalled:(blockBLEResult)callback;
- (void)stalled:(blockBLEResult)callback voiceCfg:(XCVoiceConfig *)voiceConfig;


/**
 播放语音
 @param index 语音对应的Index
 */
- (void)playSound:(NSInteger)index callback:(blockBLEResult)callback;
- (void)playSound:(NSInteger)index volume:(NSInteger)volume callback:(blockBLEResult)callback;


/**
 BLE配置APN名称

 @param apnName 'APN,name,passwd' 其中name 和 password 可以为空 ""
 @param callback 结果回调
 */
- (void)setAPN:(NSString *)apnName userName:(NSString *)userName password:(NSString *)password callback:(blockBLEResult)callback;



/// 道钉设防
/// @param idx 语音位
/// @param volume 音量
/// @param callback 回调
- (void)tBeaconDefend:(NSInteger)idx volume:(NSInteger)volume callback:(blockBLEResult)callback;


/// 获取最后一次道钉信息
/// @param callback 回调
- (void)getLasttBeaconInfo:(blockBLEResult)callback;


/// 开头盔锁
/// @param callback 回调
- (void)startHelmet:(blockBLEResult)callback;

/**
 获取蓝牙适配器状态 所有非开状态返回false 需要先setBlockOnCentralManagerDidUpdateState

 @return 蓝牙适配器状态
 */
- (BOOL)isBluetoothAdapterOn;




/**
 设置连接设备的RSSI监听 该项需要在 Configuration 中配置rssiInterval 默认为0 关闭

 @param callback 从callback中返回
 */
-(void)setBlockReadRSSI:(XCBLEDidReadRSSIBlock)callback;


/**
 设置发现设备的监听
 
 @param block 在block中执行对应操作
 */
- (void)startDiscoverToPeripherals:(XCBLEDiscoverPeripheralsBlock)block;

@end
