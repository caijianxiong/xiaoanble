
//  BLEScannerTableViewController.m
//  BLETest
//
//  Created by jianxiongc on 2018/12/4.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import "BLEScannerTableViewController.h"
#import <XCBLETool/XCBLETool.h>
#import "BLEScannerCell.h"

const NSInteger maxCount = 10;

@interface BLEScannerTableViewController ()

@property (nonatomic,strong) NSMutableArray *deviceArray;
@property (nonatomic,strong) NSMutableArray *deviceIdArray;

@property (nonatomic,assign) BOOL needReload;

@end

@implementation BLEScannerTableViewController

-(NSMutableArray *)deviceArray {
    if (!_deviceArray) {
        _deviceArray = [[NSMutableArray alloc] init];
    }
    return _deviceArray;
}

- (NSMutableArray *)deviceIdArray {
    if (!_deviceIdArray) {
        _deviceIdArray = [[NSMutableArray alloc] init];
    }
    return _deviceIdArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"BLEScannerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BLEScannerCell"];
//    self.view.backgroundColor = [UIColor colorWithRed:54./256 green:55./256 blue:77./256 alpha:1];
    self.tableView.tableFooterView = [UIView new];
    self.needReload = true;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] startDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
        if (weakSelf.needReload) {
            NSNumber *filterRSSI = [[XCBLEUnits shareInstance] filterRSSI:RSSI deviceId:peripheral.identifier.UUIDString];
            [weakSelf insertTableView:peripheral advertisementData:advertisementData RSSI:filterRSSI];
        }
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[XCBLEMain shared] stopScan];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return self.deviceArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BLEScannerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BLEScannerCell"];
    NSDictionary *item = [self.deviceArray objectAtIndex:indexPath.row];
    CBPeripheral *peripheral = [item objectForKey:@"peripheral"];
    NSDictionary *advertisementData = [item objectForKey:@"advertisementData"];
    //peripheral的显示,优先用imei的定义，若没有再使用peripheral name
    NSString *imei;
    if ([advertisementData objectForKey:@"kCBAdvDataLocalName"]) {
//        peripheralName = [NSString stringWithFormat:@"%@",[advertisementData objectForKey:@"kCBAdvDataLocalName"]];
        NSData *data = [advertisementData valueForKey:CBAdvertisementDataManufacturerDataKey];
        imei = [self inlinDataToNumberStr:data];
        
    }else if(!([peripheral.name isEqualToString:@""] || peripheral.name == nil)){
        imei = peripheral.name;
    }else{
        imei = [peripheral.identifier UUIDString];
    }
    
    cell.deviceIdLabel.text = imei;
    cell.promptLabel.text = item[@"state"];
    
    return cell;    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


#pragma mark -UIViewController 方法
//插入table数据
-(void)insertTableView:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    
    NSArray *peripherals = [self.deviceArray valueForKey:@"peripheral"];
    NSData *data = [advertisementData valueForKey:CBAdvertisementDataManufacturerDataKey];
    NSString *imei = [self inlinDataToNumberStr:data];
    
    if(![self.deviceIdArray containsObject:imei]) {
        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:peripherals.count inSection:0];
        [indexPaths addObject:indexPath];
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item setValue:peripheral forKey:@"peripheral"];
        [item setValue:RSSI forKey:@"RSSI"];
        [item setValue:RSSI forKey:@"AvgRSSI"];
        [item setValue:[NSNumber numberWithInteger:1] forKey:@"AvgCount"];
        [item setValue:@"正在靠近" forKey:@"state"];
        [item setValue:advertisementData forKey:@"advertisementData"];
        [item setValue:0 forKey:@"oldAvgRSSI"];
        [self.deviceArray addObject:item];
        [self.deviceIdArray addObject:imei];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }else {
        
        NSUInteger index = [self.deviceIdArray indexOfObject:imei];
        //查看上次的RSSI值
        NSDictionary *oldItem = [self.deviceArray objectAtIndex:index];
        NSInteger oldAvgCount = [oldItem[@"AvgCount"] integerValue];
        if (oldAvgCount >= maxCount) {
            NSInteger avgRSSI = [oldItem[@"AvgRSSI"] integerValue] / maxCount;
            NSInteger oldAvgRSSI = [oldItem[@"oldAvgRSSI"] integerValue];
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            if (oldAvgRSSI <= avgRSSI + 1 && oldAvgRSSI >= avgRSSI - 1) {
                [item setValue:oldItem[@"state"] forKey:@"state"];
            } else if (avgRSSI >= oldAvgRSSI) {
                [item setValue:@"正在靠近" forKey:@"state"];
            } else {
                [item setValue:@"正在远离" forKey:@"state"];
            }
            oldAvgRSSI = avgRSSI;
            [item setValue:peripheral forKey:@"peripheral"];
            [item setValue:RSSI forKey:@"RSSI"];
            [item setValue:advertisementData forKey:@"advertisementData"];
            [item setValue:RSSI forKey:@"AvgRSSI"];
            [item setValue:[NSNumber numberWithInteger:1] forKey:@"AvgCount"];
            [item setValue:[NSNumber numberWithInteger:oldAvgRSSI] forKey:@"oldAvgRSSI"];
            [self.deviceArray replaceObjectAtIndex:index withObject:item];
            [self.tableView reloadData];
        } else {
            
            NSInteger avgRSSI = [RSSI integerValue] + [oldItem[@"AvgRSSI"] integerValue];
            NSInteger avgCount = [oldItem[@"AvgCount"] integerValue] + 1;
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            [item setValue:peripheral forKey:@"peripheral"];
            [item setValue:RSSI forKey:@"RSSI"];
            [item setValue:advertisementData forKey:@"advertisementData"];
            [item setValue:[NSNumber numberWithInteger:avgRSSI] forKey:@"AvgRSSI"];
            [item setValue:[NSNumber numberWithInteger:avgCount] forKey:@"AvgCount"];
            [item setValue:oldItem[@"state"] forKey:@"state"];
            [item setValue:oldItem[@"oldAvgRSSI"] forKey:@"oldAvgRSSI"];
            [self.deviceArray replaceObjectAtIndex:index withObject:item];
            [self.tableView reloadData];
        }
        


    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.needReload = false;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.needReload = true;
    });
}


-(NSString *)inlinDataToNumberStr:(NSData *)data{
    if (!data || [data length] == 0) {
        return @"";
    }
    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[data length]];
    
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        unsigned char *dataBytes = (unsigned char*)bytes;
        for (NSInteger i = 0; i < byteRange.length; i++) {
            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
            if ([hexStr length] == 2) {
                [string appendString:hexStr];
            } else {
                [string appendFormat:@"0%@", hexStr];
            }
        }
    }];
    return string;
}



@end
