//
//  BLEScannerCellTableView.h
//  BLETest
//
//  Created by jianxiongc on 2018/12/4.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BLEScannerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *promptLabel;
@end

NS_ASSUME_NONNULL_END
