//
//  BLEMenuViewController.m
//  BLETest
//
//  Created by jianxiongc on 2018/12/4.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import "BLEMenuViewController.h"
#import "BLEMenuCell.h"
#import "BLEMenuHeaderView.h"
#import "XCBLETool.h"

#define XCSreenWidth [UIScreen mainScreen].bounds.size.width
#define XCSreenHeight [UIScreen mainScreen].bounds.size.height


@interface BLEMenuViewController () <UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic,strong) UICollectionViewLayout *collectionViewLayout;

@end

@implementation BLEMenuViewController

- (NSArray *)items {
    return @[@{@"name":@"寻车",@"func":@"findCar"},@{@"name":@"推车",@"func":@"stalled"},@{@"name":@"查询",@"func":@"getInfomation"},@{@"name":@"重启",@"func":@"restart"},@{@"name":@"关机",@"func":@"shutdown"},@{@"name":@"设防",@"func":@"defendOn"},@{@"name":@"撤防",@"func":@"defendOff"}];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:true];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat itemW = [UIScreen mainScreen].bounds.size.width / 4 - 1;
    CGFloat itemH = itemW;
    layout.itemSize = CGSizeMake(itemW, itemH);
    layout.sectionInset = UIEdgeInsetsMake(1,1,1,1);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 0;
    [self.collectionView setCollectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"BLEMenuCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"BLEMenuCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"BLEMenuHeaderView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BLEMenuHeaderView"];
}


- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    BLEMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BLEMenuCell" forIndexPath:indexPath];
    cell.titleLabel.text = [self items][indexPath.row][@"name"];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        BLEMenuHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"BLEMenuHeaderView" forIndexPath:indexPath];
        return headerView;
    }
    return nil;
}

// MARK: 处理Header/Footer

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(XCSreenWidth, 200);
}


- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self items].count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:true];
//    NSString *fuc =
//    [self performSelector:@selector(fuc) withObject:nil];
    
    SEL funSel = NSSelectorFromString([self items][indexPath.row][@"func"]);
    if (funSel) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:funSel];
#pragma clang diagnostic pop
    }
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
    
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    
}

- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
    return CGSizeMake(100, 100);
}

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    return;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    
}

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    
}

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator  API_AVAILABLE(ios(9.0)){
    
}

- (void)setNeedsFocusUpdate {
    
}

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context  API_AVAILABLE(ios(9.0)){
    return true;
}

- (void)updateFocusIfNeeded {
    
}



#pragma mark - func

- (void)findCar {
    [[XCBLEMain shared] findCar:^(XCBLEResponesModel *result) {
        
    }];
}

- (void)stalled {
    [[XCBLEMain shared] stalled:^(XCBLEResponesModel *result) {
        
    }];
}

- (void)getInfomation {
    [[XCBLEMain shared] getStatus:^(XCBLEResponesModel *result) {
        
    }];
}

- (void)restart {
    [[XCBLEMain shared] restart:^(XCBLEResponesModel *result) {
        
    }];
}

- (void)shutdown {
    [[XCBLEMain shared] shutdown:^(XCBLEResponesModel *result) {
        
    }];
}

- (void)defendOn {
    [[XCBLEMain shared] setDefend:true callback:^(XCBLEResponesModel *result) {
        
    }];
}

- (void)defendOff {
    [[XCBLEMain shared] setDefend:false callback:^(XCBLEResponesModel *result) {
        
    }];
}




@end
