//
//  BLECloseToOpenBike.m
//  BLETest
//
//  Created by jianxiongc on 2018/12/5.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import "BLECloseToOpenBike.h"
#import "XCBLETool.h"
@interface BLECloseToOpenBike ()

@property (nonatomic,assign) double rssiThreshold;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (nonatomic,strong) NSMutableArray *handledArr;
@property (nonatomic,assign) BOOL waitConnect;
@property (weak, nonatomic) IBOutlet UILabel *openNumber;

@end

@implementation BLECloseToOpenBike

-(NSMutableArray *)handledArr {
    if (!_handledArr) {
        _handledArr = [NSMutableArray array];
    }
    return _handledArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.waitConnect = true;
    self.rssiThreshold =  ((1.5 * self.slider.value) + 48) / 99;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self autoOpenBike];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[XCBLEMain shared] stopScan];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"keyPath %@",keyPath);
}


- (void)autoOpenBike {
    [[XCBLEMain shared] startDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisemenData, NSNumber *RSSI) {
        __weak typeof(self)weakSelf = self;
        
        NSData *data = [advertisemenData valueForKey:CBAdvertisementDataManufacturerDataKey];
        NSString *imei = [self inlinDataToNumberStr:data];
        RSSI =  [[XCBLEUnits shareInstance] filterRSSI:RSSI deviceId:imei];
        
        NSLog(@"imei %@ RSSI %f",imei,[self calcDistance:RSSI]);
        if ([self calcDistance:RSSI] < self.rssiThreshold && ![self.handledArr containsObject:imei] && self.waitConnect) {
            self.waitConnect = false;
            
            __block BOOL conRet = false;
            __block NSDictionary *deviceStatus;
            
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            dispatch_group_t group = dispatch_group_create();
            dispatch_queue_t queue = dispatch_queue_create(0, 0);
            
            dispatch_group_async(group, queue, ^{
                //1. 连接
                [[XCBLEMain shared] connectToIMEI:[@"868" stringByAppendingString:imei] deviceReadyCallback:^(XCBLEResponesModel *result) {
                    conRet = true;
                    dispatch_semaphore_signal(semaphore);
                } fail:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
                    conRet = false;
                    dispatch_semaphore_signal(semaphore);
                }];
            });
            
            dispatch_group_notify(group, queue, ^{
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                NSLog(@"%d",conRet);
                if (conRet) {
                    //3.开车
                    [[XCBLEMain shared] setDefend:0 callback:^(XCBLEResponesModel *result) {
                        if (result.code == BleErrorCode_success) {
                            [weakSelf.handledArr addObject:imei];
                            weakSelf.openNumber.text = [NSString stringWithFormat:@"%lu",(unsigned long)weakSelf.handledArr.count];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                weakSelf.waitConnect = true;
                                [[XCBLEMain shared] cancelAllPeripheralsConnection];
                                [weakSelf autoOpenBike];
                            });
                        }else {
                            weakSelf.waitConnect = true;
                        }
                    }];
                } else {
                    self.waitConnect = true;
                }
            });
        }
    }];
}

- (double)calcDistance:(NSNumber *)rssiValue{
    NSInteger iRssi = abs([rssiValue intValue]);
    double power = (iRssi - 59) / (10 * 2.0);
    double ret = pow(10, power);
    return ret;
}

- (IBAction)sliderChange:(UISlider *)sender {
    self.rssiThreshold = ((1.5 * sender.value) + 48) / 99;
}

-(NSString *)inlinDataToNumberStr:(NSData *)data{
    NSString *string = [data description];
    NSMutableString *result = [NSMutableString stringWithFormat:@""];
    for(int i=0;i < string.length;i++){
        char c = [string characterAtIndex:i];
        if ((c>='0'&&c<='9')||(c>='a'&&c<='z')) {
            [result appendFormat:@"%c",c];
        }
    }
    return result;
}
@end
