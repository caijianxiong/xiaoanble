//
//  ViewController.m
//  BLETest
//
//  Created by 蔡剑雄 on 29/08/2018.
//  Copyright © 2018 蔡剑雄. All rights reserved.
//

#import "ViewController.h"
#import "XCBLETool.h"
#import "BLEScannerTableViewController.h"
#import "BLEAutoBatteryViewController.h"
#import "BLECloseToOpenBike.h"


#define width [UIScreen mainScreen].bounds.size.width
#define height [UIScreen mainScreen].bounds.size.height

@interface ViewController ()

- (IBAction)connectIMEI:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UITextField *imeiTextField;
- (IBAction)getInformationAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *repsponeTextView;

@property (nonatomic,strong) XCBLEMain *bleMain;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
- (IBAction)changeStepper:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *playSoundIndexLabel;

@property (nonatomic,assign) NSInteger audioidx;
- (IBAction)setAPN:(id)sender;

@property (weak, nonatomic) IBOutlet UISlider *voiceSilder;
@property (weak, nonatomic) IBOutlet UITextField *voiceIdxTextFiled;

@end

@implementation ViewController

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.navigationController.navigationBar setHidden:true];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.repsponeTextView.layoutManager.allowsNonContiguousLayout = NO;
    XCBLEConfigurationBuilder *build = [XCBLEConfigurationBuilder alloc].buildConfigruation().setRssiInterval(2);
    [[XCBLEMain shared] configurtion:build.configuration];
    __weak typeof(self)weakSelf = self;
//    [[XCBLEMain shared] startDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
//        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@\n%@ --- %@",weakSelf.repsponeTextView.text,peripheral.name,[[XCBLEUnits shareInstance] filterRSSI:RSSI deviceId:peripheral.identifier.UUIDString]];
//        [weakSelf.repsponeTextView scrollRangeToVisible:NSMakeRange(weakSelf.repsponeTextView.text.length, 1)];
//    }];

    [[XCBLEMain shared] setBlockDisconnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [weakSelf.connectButton setTitle:@"Disconnect,click reconnect" forState:UIControlStateNormal];
    }];
    
//    [self.bleMain setBlockReadRSSI:^(NSNumber *RSSI, NSError *error) {}];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __weak typeof(self)weakSelf = self;

    [[XCBLEMain shared] setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        switch (central.state) {
            case CBManagerStateUnknown:
                [weakSelf.connectButton setTitle:@"Unknow" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateResetting:
                [weakSelf.connectButton setTitle:@"Restart" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStatePoweredOff:
                [weakSelf.connectButton setTitle:@"Enable" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateUnsupported:
                [weakSelf.connectButton setTitle:@"Don't support" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStateUnauthorized:
                [weakSelf.connectButton setTitle:@"Don't Authorization" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = false;
                break;
            case CBManagerStatePoweredOn:
                [weakSelf.connectButton setTitle:@"Connect" forState:UIControlStateNormal];
                weakSelf.connectButton.enabled = true;
                break;
        }
    }];
    
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.imeiTextField resignFirstResponder];    
    BOOL b =  [[XCBLEMain shared] isBluetoothAdapterOn];
    NSLog(@"%d",b);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)connectIMEI:(id)sender {
    
    if([self.connectButton.titleLabel.text isEqualToString:@"Succes,click Disconnect"]){
        [[XCBLEMain shared] cancelAllPeripheralsConnection];
    }else if([self.connectButton.titleLabel.text isEqualToString:@"Connecting,click stop"]){
        [[XCBLEMain shared] stopScan];
         [self.connectButton setTitle:@"Start connect" forState:UIControlStateNormal];
    }
    else{
        NSString *imei = self.imeiTextField.text;
        [self.connectButton setTitle:@"Connecting,click stop" forState:UIControlStateNormal];
        [[XCBLEMain shared] connectToIMEI:imei deviceReadyCallback:^(XCBLEResponesModel *result) {
            [self.connectButton setTitle:@"Succes,click Disconnect" forState:UIControlStateNormal];
        } fail:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
            [self.connectButton setTitle:@"Connect fail" forState:UIControlStateNormal];
        }];
    }
    
}
- (IBAction)getInformationAction:(id)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] getStatus:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
- (IBAction)accSwitchChange:(UISwitch *)sender {
    __weak typeof(self)weakSelf = self;
    XCVoiceConfig *voiceCfg = [[XCVoiceConfig alloc]init:[self.voiceIdxTextFiled.text integerValue] volume:self.voiceSilder.value];
    [[XCBLEMain shared] setAcc:sender.isOn callback:^(XCBLEResponesModel *result) {
         weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    } voiceConfig:voiceCfg];
}
- (IBAction)backWheelSwitchChange:(UISwitch *)sender {
    __weak typeof(self)weakSelf = self;
    
    XCVoiceConfig *voiceCfg = [[XCVoiceConfig alloc]init:[self.voiceIdxTextFiled.text integerValue] volume:self.voiceSilder.value];
    [[XCBLEMain shared] setBackWheel:sender.isOn callback:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    } voiceConfig:voiceCfg];
}
- (IBAction)seatLockSwitchChange:(UISwitch *)sender {
    __weak typeof(self)weakSelf = self;
        XCVoiceConfig *voiceCfg = [[XCVoiceConfig alloc]init:[self.voiceIdxTextFiled.text integerValue] volume:self.voiceSilder.value];
    [voiceCfg voiceConfig:[self.voiceIdxTextFiled.text integerValue] volume:self.voiceSilder.value];
    [[XCBLEMain shared] setSaddle:sender.isOn callback:^(XCBLEResponesModel *result) {
          weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    } voiceConfig:voiceCfg];
}
- (IBAction)defendSwitchChange:(UISwitch *)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] setDefend:sender.isOn callback:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
- (IBAction)setStoreModel:(UISwitch *)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] setStoreModel:sender.isOn callback:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    } voiceConfig:nil];
}
- (IBAction)restartAction:(id)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] restart:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
- (IBAction)findCarAction:(id)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] findCar:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    } volume:self.voiceSilder.value];
}
- (IBAction)shutdownAction:(id)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] shutdown:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
- (IBAction)stalledAction:(id)sender {
    __weak typeof(self)weakSelf = self;
    XCVoiceConfig *voiceCfg = [[XCVoiceConfig alloc]init:[self.voiceIdxTextFiled.text integerValue] volume:self.voiceSilder.value];
    [voiceCfg voiceConfig:[self.voiceIdxTextFiled.text integerValue] volume:self.voiceSilder.value];
    [[XCBLEMain shared] stalled:^(XCBLEResponesModel *result) {
       weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    } voiceCfg:voiceCfg];
}



- (IBAction)changeStepper:(UIStepper *)sender {
    NSInteger index = sender.value;
    __weak typeof(self)weakSelf = self;
    self.playSoundIndexLabel.text = [NSString stringWithFormat:@"%ld",(long)index];
    [[XCBLEMain shared] playSound:index callback:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
- (IBAction)connectFindCar:(id)sender {
    NSString *imei = self.imeiTextField.text;
    [[XCBLEMain shared] connectToIMEI:imei deviceReadyCallback:^(XCBLEResponesModel *result) {
        [self.connectButton setTitle:@"Succes,click Disconnect" forState:UIControlStateNormal];
        [self findCarAction:nil];        
    } fail:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [self.connectButton setTitle:@"Connect fail" forState:UIControlStateNormal];
    }];
}
- (IBAction)blescan:(id)sender {
    [self.navigationController pushViewController:[[BLEScannerTableViewController alloc] init] animated:true];
}
- (IBAction)autoBattery:(id)sender {
    [self.navigationController pushViewController:[[BLEAutoBatteryViewController alloc] init] animated:true];
}
- (IBAction)closeToOpenBike:(id)sender {
    [self.navigationController pushViewController:[[BLECloseToOpenBike alloc] init] animated:true];
}
- (IBAction)getGPSInfo:(id)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] getGPSInfo:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
- (IBAction)getDeatilInfo:(id)sender {
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] getDetailInfo:^(XCBLEResponesModel *result) {
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}

- (IBAction)setAPN:(id)sender {
    //测试最大长度 120 bytes
    __weak typeof(self)weakSelf = self;
    [[XCBLEMain shared] setAPN:@"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" userName:@"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB" password:@"CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC" callback:^(XCBLEResponesModel *result) {
        NSLog(@"---> SET APN LOG : %@",result);
        weakSelf.repsponeTextView.text = [NSString stringWithFormat:@"%@",[result description]];
    }];
}
@end
